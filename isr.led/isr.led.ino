#include <avr/interrupt.h>

int blinkinLedState = LOW;

void isrCallback() {
    //blinkinLedState = blinkinLedState == HIGH ? LOW : HIGH;
    if (blinkinLedState == HIGH) {
        blinkinLedState = LOW;
    } else {
        blinkinLedState = HIGH;
    }
    digitalWrite(LED_BUILTIN, blinkinLedState);
}

ISR(TIMER1_COMPA_vect)
{
  isrCallback();
}


// Ripped from TimerOne
// This is actually one cycle higher than supported, which is why we need to
// use < below, not <=.
#define TIMER1_RESOLUTION (65536UL)  // Timer1 is 16 bit
void setPeriod(unsigned long microseconds) {
    const unsigned long cycles = (F_CPU / 2000000) * microseconds;
    unsigned char clockSelectBits;
    unsigned short pwmPeriod;
    if (cycles < TIMER1_RESOLUTION) {
            clockSelectBits = _BV(CS10);
            pwmPeriod = cycles;
    } else
    if (cycles < TIMER1_RESOLUTION * 8) {
            clockSelectBits = _BV(CS11);
            pwmPeriod = cycles / 8;
    } else
    if (cycles < TIMER1_RESOLUTION * 64) {
            clockSelectBits = _BV(CS11) | _BV(CS10);
            pwmPeriod = cycles / 64;
    } else
    if (cycles < TIMER1_RESOLUTION * 256) {
            clockSelectBits = _BV(CS12);
            pwmPeriod = cycles / 256;
    } else
    if (cycles < TIMER1_RESOLUTION * 1024) {
            clockSelectBits = _BV(CS12) | _BV(CS10);
            pwmPeriod = cycles / 1024;
    } else {
            clockSelectBits = _BV(CS12) | _BV(CS10);
            pwmPeriod = TIMER1_RESOLUTION - 1;
    }
    // The assembler will pull the high bits into a TEMP register at the same
    // time as the low read and combine them to return the short.
    OCR1A = pwmPeriod;
    TCCR1B = _BV(WGM12) | clockSelectBits;
}

void setup() {
    pinMode(LED_BUILTIN, OUTPUT);

    // Disable global interrupts until this is complete? (p.11)
    //SREG &= ~ (1 << 7);
    cli();

    // Disconnect the output compare mode, turning off the PWM.
    // Set clear on compare match. (p.131)
    TCCR1A = _BV(COM1A1);

    // Enable interrupts on Timer/Counter1 (p.135)
    TIMSK1 = _BV(OCIE1A);

    setPeriod(500000);

    // Lastly, globally enable interrupts.
    //SREG |= 1 << 7;
    sei();
}

void loop() {
    delay(10000);
}
