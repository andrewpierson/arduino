#include "audio.samples.h"

const int outputPins[] = { 2, 3, 4, 5, 6, 7, 8, 9 };

int blinkinLedState = LOW;
bool playbackEnabled = true;
unsigned int sampleIndex = 0;
int LATCH_ON_LOW_TO_HIGH_PIN = 13;
int LATCH_NOT_CLEAR_PIN = 12;


#if 0
void writeByteToDac (unsigned char sample) {
    unsigned char bitMask = 1;
    digitalWrite(LATCH_ON_LOW_TO_HIGH_PIN, HIGH);
    for (unsigned int i=0; i<8; ++i) {
        // Apparently LOW == false == 0, so this is faster than branching
        // logic.  If the bit is set, then the int is non-zero, making it HIGH.
        // Otherwise it's LOW (all bits zero).
        int highOrLow = sample & bitMask;
        digitalWrite(outputPins[i], highOrLow);
        bitMask <<= 1;
    }
    digitalWrite(LATCH_ON_LOW_TO_HIGH_PIN, LOW);
}


void isrCallback() {
    unsigned char sample = pgm_read_byte_near(stfdAudioSamples + sampleIndex);
    // Sometimes we read crap data.  When we do, discard it.
    if (sample == 0 || sample == 0xFF) {
        return;
    }
    writeByteToDac(sample);
    ++sampleIndex;
    if (sampleIndex >= numSamples) {
        playbackEnabled = false;
        sampleIndex = 0;
        // Disable the ISR.
        TIMSK1 = 0;
    }
}

ISR(TIMER1_COMPA_vect)
{
  isrCallback();
}
#else
ISR(TIMER1_COMPA_vect)
{
    digitalWrite(LATCH_ON_LOW_TO_HIGH_PIN, LOW);

    unsigned char sample = pgm_read_byte_near(stfdAudioSamples + sampleIndex);
    ++sampleIndex;
    unsigned char bitMask = 1;

    digitalWrite(outputPins[0], sample & bitMask);
    bitMask <<= 1;
    digitalWrite(outputPins[1], sample & bitMask);
    bitMask <<= 1;
    digitalWrite(outputPins[2], sample & bitMask);
    bitMask <<= 1;
    digitalWrite(outputPins[3], sample & bitMask);
    bitMask <<= 1;
    digitalWrite(outputPins[4], sample & bitMask);
    bitMask <<= 1;
    digitalWrite(outputPins[5], sample & bitMask);
    bitMask <<= 1;
    digitalWrite(outputPins[6], sample & bitMask);
    bitMask <<= 1;
    digitalWrite(outputPins[7], sample & bitMask);
    bitMask <<= 1;

    if (sampleIndex >= numSamples) {
        playbackEnabled = false;
        digitalWrite(LATCH_NOT_CLEAR_PIN, 1);
        sampleIndex = 0;
        // Disable the ISR.
        TIMSK1 = 0;
    }

    digitalWrite(LATCH_ON_LOW_TO_HIGH_PIN, HIGH);
}
#endif

// Ripped from TimerOne
// This is actually one cycle higher than supported, which is why we need to
// use < below, not <=.
#define TIMER1_RESOLUTION (65536UL)  // Timer1 is 16 bit
void setPeriod(unsigned long microseconds) {
    const unsigned long cycles = (F_CPU / 1000000) * microseconds;
    unsigned char clockSelectBits;
    unsigned short pwmPeriod;
    if (cycles < TIMER1_RESOLUTION) {
            clockSelectBits = _BV(CS10);
            pwmPeriod = cycles;
    } else
    if (cycles < TIMER1_RESOLUTION * 8) {
            clockSelectBits = _BV(CS11);
            pwmPeriod = cycles / 8;
    } else
    if (cycles < TIMER1_RESOLUTION * 64) {
            clockSelectBits = _BV(CS11) | _BV(CS10);
            pwmPeriod = cycles / 64;
    } else
    if (cycles < TIMER1_RESOLUTION * 256) {
            clockSelectBits = _BV(CS12);
            pwmPeriod = cycles / 256;
    } else
    if (cycles < TIMER1_RESOLUTION * 1024) {
            clockSelectBits = _BV(CS12) | _BV(CS10);
            pwmPeriod = cycles / 1024;
    } else {
            clockSelectBits = _BV(CS12) | _BV(CS10);
            pwmPeriod = TIMER1_RESOLUTION - 1;
    }
    // The assembler will pull the high bits into a TEMP register at the same
    // time as the low read and combine them to return the short.
    OCR1A = pwmPeriod;
    TCCR1B = _BV(WGM12) | clockSelectBits;
}

void setup() {
    for (unsigned int i=0; i<8; ++i)
    {
        pinMode(outputPins[i], OUTPUT);
    }
    //writeByteToDac(pgm_read_byte_near(stfdAudioSamples + sampleIndex));
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(LATCH_ON_LOW_TO_HIGH_PIN, OUTPUT);
    pinMode(LATCH_NOT_CLEAR_PIN, OUTPUT);

    digitalWrite(LED_BUILTIN, blinkinLedState);
    digitalWrite(LATCH_ON_LOW_TO_HIGH_PIN, 0);
    digitalWrite(LATCH_NOT_CLEAR_PIN, 0);

    // Disable global interrupts until this is complete? (p.11)
    //SREG &= ~ (1 << 7);
    cli();

    // Disconnect the output compare mode, turning off the PWM.
    // Set clear on compare match. (p.131)
    // Only using channel A.
    TCCR1A = _BV(COM1A1);

    // Enable interrupts on Timer/Counter1 (p.135)
    TIMSK1 = _BV(OCIE1A);

    // I have sampled at 8 kHz, so the samples should be written every
    // (1,000,000 [microseconds / second] / 8000 Hz) = 125 microseconds
    setPeriod(125);

    // Lastly, globally enable interrupts.
    //SREG |= 1 << 7;
    sei();
}

void loop() {
    // put your main code here, to run repeatedly:
    if (blinkinLedState == HIGH) {
        blinkinLedState = LOW;
    } else {
        blinkinLedState = HIGH;
    }
    digitalWrite(LED_BUILTIN, blinkinLedState);
    delay(3000);
    playbackEnabled = true;
    digitalWrite(LATCH_NOT_CLEAR_PIN, 1);
    TIMSK1 = _BV(OCIE1A);
}
